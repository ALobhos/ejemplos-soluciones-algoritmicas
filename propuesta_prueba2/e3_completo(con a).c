#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//A) Para demostrar que es una celebridad con las matrices, basta que, empezando desde un numero 1 de la diagonal
//   este solo tenga 0s a sus lados, y solo tenga 1s sobre y debajo de el.

int celebridad(int n, int matriz[n][n]){

    int celebridad = 0;


    for(int i=0; i<n; i++){
        for (int j=0; j<n; j++){

            //Se revisa si existe alguna persona que solo tenga unos sobre y
            //abajo de el, y que a ambos lados solo tenga ceros
            if (i!=j){

                if(matriz[j][i] != 1){
                    celebridad = 0;
                    break;
                }
                if(matriz[i][j] != 0){
                    celebridad = 0;
                    break;
                }

                celebridad = 1;
            }
        }

        //Si se llegase a encontrar una celebridad el ciclo debe terminar, ya
        //que por definición, solo puede existir una
        if(celebridad){
            break;
        }
    }

    return celebridad;
}


void imprimir(int n, int matriz[n][n]){

    for(int i=0; i<n; i++){
        for (int j=0; j<n; j++){
            printf("|%d|", matriz[i][j]);
        }
        printf("\n");
    }

}


int main(){

    int personas, contador=0;
    srand(time(0));
    int random[5] = {0,0,1,1,1}; //para incrementar probabilidad de 1

    printf("Ingrese cantidad de personas");
    scanf("%d", &personas);

    //Si son menos de 4 personas, no se pueden ingresar 10 1s
    if (personas < 4){
        main();
        system("exit");
    }

    int conocidos[personas][personas];

    //Se llena matriz con unos y ceros, generando una matriz de identidad
    for(int i=0; i<personas; i++){
        for (int j=0; j<personas; j++){

            if (i == j){
                conocidos[i][j] = 1;
            }
            else{
                conocidos[i][j] = 0;
            }

            printf("|%d|", conocidos[i][j]);
        }
        printf("\n");
    }

    printf("\n");


    //Se llena con 1s aleatoriamente sin contar los de la diagonal
    while(contador < 10){

        for(int i=0; i<personas; i++){
            for (int j=0; j<personas; j++){

                if (i!=j){
                    conocidos[i][j] = random[rand()%5];

                    if (conocidos[i][j] == 1){
                        contador++;
                    }
                }
                if(contador == 10){
                    break;
                }
            }

            if(contador == 10){
                break;
            }
        }
    }

    imprimir(personas, conocidos);


    //Se revisa la existencia de una celebridad
    if(celebridad(personas, conocidos)){
        printf("Existe una celebridad");
    }
    else{
        printf("No existe una celebridad");
    }

    return 0;
}
