void asignar_unos(int tamano, int matriz[tamano][tamano]){

    int contador = 0;

    //Genera exáctamente 10 unos
    while(contador < 10){

        for (int i=0; i<tamano; i++){
            for (int j=0; j<tamano; j++){

                if (i==j){
                    matriz[i][j] = 1;
                }

                else{
                    matriz[i][j] = rand()%2;

                    if(matriz[i][j] == 1){
                        contador++;
                    }
                }

                if(contador == 10){
                    break;
                }
            }

            if(contador == 10){
                break;
            }
        }
    }
}
