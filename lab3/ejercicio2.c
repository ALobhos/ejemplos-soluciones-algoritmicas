#include <stdio.h>

void ordenar(int n1, int n2, int n3, int n4){

    if(n1 < n2){
        ordenar(n2, n1, n3, n4);
    }
    else if(n1 < n3){
        ordenar(n3, n2, n1, n4);
    }
    else if(n1 < n4){
        ordenar(n4, n2, n3, n1);
    }
    else if(n2 < n3){
        ordenar(n1, n3, n2, n4);
    }
    else if(n2 < n4){
        ordenar(n1, n4, n3, n2);
    }
    else if(n3 < n4){
        ordenar(n1, n2, n4, n3);
    }
    else{
        printf("El mayor numero que puede formar es: %d%d%d%d", n1, n2, n3, n4);
        printf("\n El menor numero que puede formar es %d%d%d%d", n4, n3, n2, n1);
    }
}


void cortador(int numero){

    int n1, n2, n3, n4;

    n1 = numero%10;
    n2 = (numero/10)%10;
    n3 = (numero/100)%10;
    n4 = (numero/1000)%10   ;

    ordenar(n1, n2, n3, n4);
}


int main(){

    int numero;

    printf ("Por favor, ingrese un numero de 4 digitos: ");
    scanf("%d", &numero);

    if(numero >= 10000 || numero <= 999){
        printf("El numero ingresado no es valido\n");
        main();
    }

    else{
        cortador(numero);
    }

    return 0;
}
