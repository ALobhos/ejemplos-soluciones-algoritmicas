#include <stdio.h>

void raiz_cubica(float numero, float raiz, int decimal){

    float sumar, cubo;
    sumar = 1.0/decimal;

    raiz = raiz + sumar;
    cubo = raiz*raiz*raiz;

    if(decimal == 100000){
		printf("La raiz aproximada de su numero es %.3f", raiz);
	}
    else if(cubo > numero){
		raiz_cubica(numero, raiz-sumar, decimal*10);
	}
	else if(cubo == numero){
		printf("La raiz de su numero es: %f", raiz);
	}
	else{
		raiz_cubica(numero, raiz, decimal);
	}

}

int main(){

    float numero;

    printf("Ingrese un numero para calcular su raiz cubica: ");
    scanf("%f", &numero);

    raiz_cubica(numero, 0, 1);
    return 0;
}
