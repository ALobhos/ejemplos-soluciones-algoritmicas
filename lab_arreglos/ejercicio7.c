#include <stdio.h>


void partidario(int largo){

    int arreglo[largo], partidario;
    partidario = 1; //Partidario es un booleano


    //Se llena el arreglo manualmente
    for(int i=0; i<largo; i++){
        printf("Ingrese un número para la posición %d del arreglo: ", i+1);
        scanf("%d", &arreglo[i]);
    }

    //Se usan dos bucles para contar los comparar los elementos del arreglo,
    //el primero cambia los numeros pares y el segundo los numeros impares,
    //notese que se puede usar i+=2 para indicar que cada vuelta debe sumarle
    //2 a la variable i y j
    for(int i=0; i<largo; i+=2){
        for(int j=1; j<largo; j+=2){
            if (arreglo[i] < arreglo[j]){
                partidario = 0;
            }
        }
    }

    //Se imprime el arreglo y se dice si es o no partidario
    printf("\nSu arreglo es el siguiente:\n");
    for(int i=0; i<largo; i++){
        printf("| %d |", arreglo[i]);
    }

    //Cuando se usan booleanos, se considera el 1 como "Si se cumple" o "True"
    //Mientras que el 0 se considera como "No se cumple" o "False"
    //Y no es necesario especificar si no es igual a 1 o 0
    if(partidario){
        printf("\nY es partidario\n");
    }

    else{
        printf("\nY no es partidario\n");
    }
}


int main(){

    int largo;

    printf("Ingrese un largo para su arreglo: ");
    scanf("%d", &largo);

    //Pedí un arreglo de mínimo 3 elementos para que valga la pena compararlo
    if(largo <= 2){
        printf("Debe ser un largo mayor a 2\n");
        main();
    }
    else{
        partidario(largo);
    }

    return 0;
}
