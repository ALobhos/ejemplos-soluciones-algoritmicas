#include <stdio.h>


void crear_arreglos(int largo){

    int arreglo[largo], largo_impares, aux;

    largo_impares = 0;
    aux = 0;

    for (int i=0; i<largo; i++){

        printf("Ingrese valor para el elemento %d del arreglo: ", i+1);
        scanf("%d", &arreglo[i]);

        //Creé un 2do arreglo solo debido al que el problema decía explícitamente
        //"devuelva otro arreglo"
        if(arreglo[i]%2 != 0){
            largo_impares++;
        }
    }

    int arreglo_impares[largo_impares];

    //Se llena arreglo con números impares
    for (int i=0; i<largo; i++){

        if(arreglo[i]%2 != 0){
            arreglo_impares[aux] = arreglo[i];
            aux++;
        }
    }

    //Se imprime arreglo con números impares
    printf("El arreglo que solo contiene números impares es el siguiente:\n");
    for(int i=0; i<largo_impares; i++){
        printf("| %d |", arreglo_impares[i]);
    }
    printf("\n");
}


int main(){

    int largo;

    printf("Ingrese largo para arreglo: ");
    scanf("%d", &largo);

    crear_arreglos(largo);

    return 0;
}
