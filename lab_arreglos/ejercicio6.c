#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void indices(){

    int numero, arreglo[20];

    //Se llena un arreglo de 20 elementos con números aleatorios entre 1 y 20
    printf("El arreglo generado aleatoriamente es el siguiente:\n");
    for(int i=0; i<20; i++){

        arreglo[i] = (rand()%20)+1;
        printf("| %d |", arreglo[i]);
    }

    //Se pide un numero de referencia y se busca si algun elemento del arreglo
    //es igual, en caso de serlo, se entrega su índice
    printf("\nIngrese un número para buscar en el arreglo: ");
    scanf("%d", &numero);

    printf("Su número se encuentra en los siguientes índices del arreglo:\n");
    for(int i=0; i<20; i++){

        if(arreglo[i] == numero){
            printf("| %d |", i);
        }
    }
    printf("\n");
}

int main(){

    srand(time(0));
    indices();

    return 0;
}
