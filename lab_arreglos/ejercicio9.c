#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void compras(){

    int productos[10], pedidos[10], compras[10];

    //Se generan 10 cantidades aleatorias entre 1 y 20
    printf("Se tienen las siguientes cantidades:\n");
    for(int i=0; i<10; i++){

        productos[i] = (rand()%20)+1;
        printf("%d unidades del producto %d\n", productos[i], i+1);
    }

    printf("\n########################################################\n\n");


    //Se pregunta cuantas unidades llevará de cada producto, y se aprovecha el
    //mismo ciclo para comparar cantidades y llenar el tercer arreglo
    for(int i=0; i<10; i++){
        printf("¿Cuantas unidades desea pedir del producto %d? ", i+1);
        scanf("%d", &pedidos[i]);

        if(pedidos[i] == productos[i]){
            compras[i] = pedidos[i];
        }
        else if(pedidos[i] > productos[i]){
            compras[i] = (pedidos[i] - productos[i])*2;
        }
        else{
            compras[i] = pedidos[i];
        }
    }

    printf("\n########################################################\n\n");

    //Se imprime la cantidad necesaria de cada producto
    printf("Se requieren comprar las siguientes cantidades para mantener stock\n");
    for(int i=0; i<10; i++){
        printf("%d unidades del producto %d\n", compras[i], i+1);
    }
}


int main(){

    srand(time(0));
    compras();

    return 0;
}
