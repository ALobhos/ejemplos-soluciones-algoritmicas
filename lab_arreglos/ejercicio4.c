#include <stdio.h>


void crear_arreglo(int largo){

    int arreglo[largo], i;
    i = 1;

    //Se pregunta por el término inicial fuera de cualquier bucle
    printf("Ingrese un número para la posición 1 del arreglo: ");
    scanf("%d", &arreglo[0]);

    //En vez de usar un ciclo for, se utiliza un ciclo while ya que así se puede
    //controlar mas fácil cuando el índice aumenta y cuando no
    while(i<largo){

        printf("Ingrese un número para la posición %d del arreglo: ", i+1);
        scanf("%d", &arreglo[i]);

        //Solo si el número es válido (mayor que el anterior), se suma uno al
        //índice y se pasa al siguiente elemento
        if (arreglo[i] <= arreglo[i-1]){
            printf("Debe ingresar un numero mayor que %d\n", arreglo[i-1]);
        }
        else{
            i++;
        }
    }

    //Se imprime el arreglo
    printf("Su arreglo es:\n");
    for (i=0; i<largo; i++){
        printf("| %d |", arreglo[i]);
    }
    printf("\n");
}


int main(){

    int largo;

    printf("¿Cual será el largo de su arreglo?\n");
    scanf("%d", &largo);

    crear_arreglo(largo);

    return 0;
}
