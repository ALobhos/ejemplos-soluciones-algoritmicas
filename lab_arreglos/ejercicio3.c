#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void comparar_arreglo(int largo, int referencia){

    int arreglo[largo];

    //Se le da a cada elemento del arreglo un valor aleatorio, tiene como límite
    //el doble del número de referencia aumentado en 10 (solo fue una corazonada
    //mía para que no fueran valores tan locos, no es necesario que presten mayor
    //atención a eso último)
    //Nótese que el mismo bucle que imprime el arreglo es tambien el que lo llena
    printf("El arreglo generado aleatoriamente fue:\n");
    for (int i=0; i<largo; i++){
        arreglo[i] = (rand()%(referencia*2 + 10))+1;
        printf("| %d |", arreglo[i]);
    }


    //Cada comparación imprime sus valores
    printf("\nLos elementos mayores al número de referencia son:\n");
    for (int i=0; i<largo; i++){
        if(arreglo[i] > referencia){
            printf("| %d |", arreglo[i]);
        }
    }

    printf("\nLos elementos menores al número de referencia son:\n");
    for (int i=0; i<largo; i++){
        if(arreglo[i] < referencia){
            printf("| %d |", arreglo[i]);
        }
    }

    printf("\nLos elementos iguales al número de referencia son:\n");
    for (int i=0; i<largo; i++){
        if(arreglo[i] == referencia){
            printf("| %d |", arreglo[i]);
        }
    }

    printf("\nLos múltiplos del número de referencia son:\n");
    for (int i=0; i<largo; i++){
        if(arreglo[i]%referencia == 0){
            printf("| %d |", arreglo[i]);
        }
    }
    printf("\n");

}

int main(){

    int largo, numero;

    printf("Ingrese largo para arreglo: ");
    scanf("%d", &largo);

    printf("Ingrese un número de referencia: ");
    scanf("%d", &numero);

    srand(time(0));
    comparar_arreglo(largo, numero);

    return 0;
}
