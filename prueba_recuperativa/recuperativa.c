#include <stdio.h>

//Función que revisa si el número actual es múltiplo de n
int multiplos(int actual, int n){


    //0 y 1 se comportan como booleano, 1 representa true y 0 false
    int verdadero = 0;

    if(actual%n == 0){
	verdadero=1;
    }

    return verdadero;
}

//Función que imprime cuadrado (matriz)
void imprimir_cuadrado(int n, int cuadrado[n][n]){


    for(int i=0; i<n; i++){
	for(int j=0; j<n; j++){
	    printf("|%d|", cuadrado[i][j]);
	}
	printf("\n");
    }
}


//Función generadora del cuadrado
void generar_cuadrado(int n){

    //Siempre se parte desde 1
    int actual=1;
    int columna, fila;
    int cuadrado[n][n];
    int multiplo = 0;

    //Se debe llegar al cuadrado del número seleccionado
    while(actual <=(n*n)){

	//Condición (a)
	if(actual == 1){
	    fila = 0; //Primera fila
            columna = (n/2); //Dado que son matrices, esto da la columna central cuando es n es impar
	    cuadrado[fila][columna] = actual;
	}

	else{

	    //Condición (D), no aplica (B)
	    if(multiplo){
		
		//Condicion (C)
		if(fila == (n-1)){
		    fila = 0;
		}
		else{
		    fila++;
		}
		cuadrado[fila][columna] = actual;
		multiplo = 0;
	    }

	    else{

		//Condición (B) solo si el número anterior no era múltiplo de n
	        if(fila == 0){ // Condición (C)
		    fila = n-1;
	        }
                else{
		    fila--;
	        }

                if(columna == (n-1)){ //Condición (C)
		    columna = 0;
	        }
	        else{
		    columna++;
	        }

	        cuadrado[fila][columna] = actual;
		//Se evalúa si el actual es múltiplo de n, para asi no aplicar B en siguiente pasada
		multiplo = multiplos(actual, n);
	    }
	}

	actual++;
    }

    imprimir_cuadrado(n, cuadrado);
}
    

//Función que pide datos
void cuadrado_magico(){

    int n;
    
    printf("¿De que dimensión desea generar un cubo?");
    scanf("%d", &n);

    //Se valida que sea un n posivo impar
    if(n<1 || n%2==0){

        printf("Número no válido (debe ser positivo impar)");
        cuadrado_magico();
    }

    else{

        generar_cuadrado(n);
    }
}


int main(){

   cuadrado_magico();


   return 0;
}
